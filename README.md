# Squad

## Teste para novos programadores backend da Squad

- O WebService REST será utilizado para cadastrar e buscar Cidades e Estados.
Crie o modelo de dados,endpoints e testes funcionais necessários que satisfaçam os cenários abaixo:

    - Chamada que cadastra uma cidade em um estado

    - Chamada que cadastra uma cidade em um estado mas a cidade ja existe para o estado

    - Busca de cidade retornando 1 resultado, multiplos resultados e nenhum resultado

    - Busca de cidade com usuário passando um caracter inválido: Ex (São] Paulo) - A string deverá ser normalizada antes da busca pela cidade.


- Documente a API
- Pratique a metodologia [12 factor cloud native](https://12factor.net/)
- Escreva boas mensagens de commit
- Utilize todas as boas práticas de programação que você conhece
- Crie um docker compose com a stack do app:
  - Django como Web Tier
  - Postgress como Backing Service
- Faça uma breve descrição do projeto e de como rodar os testes
- O ambiente de desenvolvimento deverá ser ativado preferencialmente com um comando.
- Faça um fork deste projeto e quando terminar, dê acesso de leitua aos usuários squadbr e rafagonc


Software Stack:

- [Python 3.6](https://www.python.org/downloads/release/python-360/) 

- [Django](https://www.djangoproject.com/)

- [Rest Framework](http://www.django-rest-framework.org/)

- [PostgreSQL](https://www.postgresql.org/)

- [Docker](https://www.docker.com/)